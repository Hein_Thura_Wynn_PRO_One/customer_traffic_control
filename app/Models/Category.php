<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'main_category', 
        'product_category_code', 'product_category_name',
        'product_group_code', 'product_group_name', 
        'product_pattern_code','product_pattern_name',
        'product_design_code','product_design_name',
        'type'
    ];
}


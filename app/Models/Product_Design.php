<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Design extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'product_designs';
    protected $fillable = [
        'product_pattern_id', 'product_design_id', 'product_design_code', 'product_design_name'
    ];
}

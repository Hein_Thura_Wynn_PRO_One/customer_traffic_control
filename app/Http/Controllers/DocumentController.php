<?php

namespace App\Http\Controllers;

use App\Exports\DocumentExport;
use App\Models\Brand;
use App\Models\Document;
use App\Models\Product_Category;
use App\Models\Product_Design;
use App\Models\Product_Group;
use App\Models\Product_Pattern;
use App\Models\ProductCode;
use App\Models\Supplier;
use App\Models\Unit;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:document-list|document-create|document-edit|document-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:document-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:document-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:document-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        // $u_id = Auth::id();
        // $u_obj = User::select('mercate_id')->where('id', $u_id)->first();
        // $u_cat = $u_obj->mercate_id;
        // if ($u_cat) {
        //     $category = Product_Category::where('id', $u_cat)->first();
        //     $categories = null;
        // } else {
        //     $categories = Product_Category::query()->get();
        //     $category = null;
        // }
        // $vendors = Supplier::query()->get();
        // $brands = Brand::query()->get();
        // $units = Unit::query()->get();
        // $products = ProductCode::query()->get();
        // return view('documents.index',compact('products','categories', 'category', 'brands', 'vendors', 'units'));
        $documents = Document::query()->get();
        return view('documents.index', compact('documents'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $u_id = Auth::id();
        // $u_obj = User::select('mercate_id')->where('id', $u_id)->first();
        // $u_cat = $u_obj->mercate_id;
        // if ($u_cat) {
        //     $category = Product_Category::where('id', $u_cat)->first();
        //     $categories = null;
        // } else {
        //     $categories = Product_Category::query()->get();
        //     $category = null;
        // }
        // $vendors = Supplier::query()->get();
        // $brands = Brand::query()->get();
        // $units = Unit::query()->get();
        // return view('documents.create', compact('categories', 'category', 'brands', 'vendors', 'units'));
        return view('documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'document_no' => 'required',
            'pcode_id' => 'required',
            'prepared_by' => 'required',
            'approved_by' => 'required',
            'exported_by' => 'required',
            'checked_by' => 'required'
        ]);

        Document::create($request->all());

        return redirect()->route('documents.index')
            ->with('success', 'Document created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        return view('documents.show', compact('document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        return view('documents.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        request()->validate([
            'document_no' => 'required',
            'pcode_id' => 'required',
            'prepared_by' => 'required',
            'approved_by' => 'required',
            'exported_by' => 'required',
            'checked_at' => 'required',
            'checked_by' => 'required'
        ]);
        $update['document_no'] = $request->get('document_no');
        $update['pcode_id'] = $request->get('pcode_id');
        $update['status'] = $request->get('status');
        $update['prepared_by'] = $request->get('prepared_by');
        $update['prepared_at'] = $request->get('prepared_at');

        $update['checked_at'] = $request->get('checked_at');
        $update['checked_by'] = $request->get('checked_by');
        $update['approved_by'] = $request->get('approved_by');
        $update['approved_at'] = $request->get('approved_at');
        $update['exported_by'] = $request->get('exported_by');
        $update['exported_at'] = $request->get('exported_at');
        
        $document->update($request->all());

        return redirect()->route('documents.index')
            ->with('success', 'Document updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        $document->delete();

        return redirect()->route('documents.index')
            ->with('success', 'Document deleted successfully');
    }
    /////DropdownAjax//////
    public function get_groups($cat_id)
    {
        $data = Product_Group::where('product_category_id', $cat_id)->get();
        // dd($data);
        Log::info($data);
        return response()->json(['data' => $data]);
    }
    public function get_patterns($gp_id)
    {
        $data = Product_Pattern::where('product_group_id', $gp_id)->get();
        // dd($data);
        Log::info($data);
        return response()->json(['data' => $data]);
    }
    public function get_designs($pat_id)
    {
        $data = Product_Design::where('product_pattern_id', $pat_id)->get();
        // dd($data);
        Log::info($data);
        return response()->json(['data' => $data]);
    }

    public function export(Request $request)
    {
        $request = DB::table('documents')->select('id')
        ->where('status', '=', "2")
        ->get();
        return Excel::download(new DocumentExport, 'documents.xlsx');
        // return Response::txt()->with($document);
    }

    // public function getDownload(Request $request){

    //     $logs = Log::all();

    //     $txt = "Logs \n";


    //     foreach ($logs as $log) {
    //         $txt .= $logs->id;
    //         $txt .= "\n";
    //     }

    //     //offer the content of txt as a download (logs.txt)
    //     return response($content)
    //                     ->withHeaders([
    //                         'Content-Type' => 'text/plain',
    //                         'Cache-Control' => 'no-store, no-cache',
    //                         'Content-Disposition' => 'attachment; filename="logs.txt',
    //                     ]);

    //     }


}

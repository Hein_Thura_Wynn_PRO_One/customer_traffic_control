<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Customer;
use App\Models\Field;
use App\Models\Period;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Reader\Xls\RC4;

class CustomerController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:customer-create', ['only' => ['index', 'show']]);
        $this->middleware('permission:customer-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:customer-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:customer-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_id = Auth::id();
        $u_obj = User::select('branch_id')->where('id', $current_id)->first();
        $u_branch_id = $u_obj->branch_id;
        if ($u_branch_id == 7) {
            $branches = Branch::where('id', '=', 7)->orwhere('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();
            $customers = Customer::orderBy('date_client', 'desc')->paginate(5);
            // $periods = Period::orderBy('id', 'asc')->paginate(10);


            $branch_id = '';
        } else {
            $branches = Branch::where('id', '=', $u_branch_id)->where('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();
            $branch = Branch::where('id', '=', $u_branch_id)->where('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->first();
            $branch_id = $u_branch_id;
            $b_code =  $branch->id;
            $customers = Customer::where('branch_id', '=', $b_code)->orderBy('date_client', 'desc')->paginate(5);
        }
        // $branches = Branch::where('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();
        // $periods =  Period::orderBy('time', 'desc');
        $periods = Period::orderBy('id', 'asc')->paginate(20);
        // $periods =  Period::all();
        $customers_field = Field::where('type', '=', '0')->get();
        $vehicles_field = Field::where('type', '=', '1')->get();
        $from_date = '';
        $to_date = '';
        $time = '';
        return view('customers.index', compact('customers', 'branches', 'periods', 'customers_field', 'vehicles_field', 'from_date', 'to_date', 'time', 'branch_id'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function search(Request $request)
    {
        $current_id = Auth::id();
        $u_obj = User::select('branch_id')->where('id', $current_id)->first();
        $u_branch_id = $u_obj->branch_id;
        $branch_id = $u_branch_id;

        $customers_field = Field::where('type', '=', '0')->get();
        $vehicles_field = Field::where('type', '=', '1')->get();
        if ($u_branch_id == 7) {
            $branches = Branch::where('id', '=', 7)->orwhere('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();
        } else {
            $branches = Branch::where('id', '=', $u_branch_id)->where('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();
        }
        // $branches = Branch::where('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();

        $input = $request->all();
        isset($input['branch_code']) ? $branch_code = trim($input['branch_code']) : $branch_code = null;
        isset($input['time']) ? $time = trim($input['time']) : $time = null;
        isset($input['from_date']) ? $from_date = trim($input['from_date']) : $from_date = date('Y-m-d');
        isset($input['to_date']) ? $to_date = trim($input['to_date']) : $to_date = date('Y-m-d');

        $customers = Customer::query();

        if ($branch_code != null) {
            $customers = $customers->where('branch_code', '=', $branch_code);
        }
        if ($from_date != null || $to_date != null) {
            $customers = $customers->whereBetween('date_client', [$from_date, $to_date]);
        }
        if ($time != null) {
            $customers = $customers->where('time', '=', $time);
        }
        // dd($customers);

        $customers = $customers->orderBy('date_client', 'desc')->paginate(5);
        $periods =  Period::all();
        $fields = Field::all();
        return view('customers.index', compact('customers', 'branches', 'periods', 'fields', 'from_date', 'to_date', 'time', 'branch_code', 'branch_id', 'customers_field', 'vehicles_field'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $current_id = Auth::id();
        $u_obj = User::select('branch_id')->where('id', $current_id)->first();
        $u_branch_id = $u_obj->branch_id;
        if ($u_branch_id == 7) {
            $branches = Branch::where('id', '=', 7)->orwhere('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();
        } else {
            $branches = Branch::where('id', '=', $u_branch_id)->where('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();
        }
        // $branches = Branch::where('branch_active', '=', 'true')->orderBy('branch_code', 'asc')->get();
        $periods = Period::orderBy('id', 'asc')->paginate(20);
        // $periods =  Period::all();
        $fields = Field::all();
        return view('customers.create', compact('branches', 'periods', 'fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current_id = Auth::id();
        $u_obj = User::select('employee_id')->where('id', $current_id)->first();
        $empl_code = $u_obj->employee_id;

        $input = $request->all();
        $primary_data = array();
        $primary_data['_token'] = $input['_token'];
        $primary_data['branch_id'] = trim($input['branch_id']);
        $primary_data['time'] = trim($input['time']);
        $primary_data['date_client'] = trim($input['date_client']);

        unset($input['_token'], $input['branch_id'], $input['time'], $input['date_client']);
        $cus_arr = array();
        $veh_arr = array();
        $tot_cus = 0;
        $tot_veh = 0;

        foreach ($input as $key => $val) {
            $key_arr = explode("-", $key);
            $val_trim = intval(ltrim($val, '0'));

            if ($key_arr[0] == 'Customer') {

                $cus_arr[$key_arr[1]] = $val_trim;
                $tot_cus += $val_trim;
            } else {
                $veh_arr[$key_arr[1]] =  $val_trim;
                $tot_veh += $val_trim;
            }
        }

        $cus_json = json_encode($cus_arr, JSON_UNESCAPED_SLASHES);
        $veh_json = json_encode($veh_arr, JSON_UNESCAPED_SLASHES);
        unset($input);

        $input = array();
        $input = $primary_data;
        $input['customers'] = $cus_json;
        $input['vehicles'] = $veh_json;
        $input['tot_cus'] = $tot_cus;
        $input['tot_veh'] = $tot_veh;
        $input['created_by'] = Auth::id();
        $input['date_server'] = now();
        $input['empl_code'] = $empl_code;
        // $data = (object) $input;
        // dd($input);

        $customer = Customer::create($input);

        return redirect()->route('customers.index')
            ->with('success', 'Customer record created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->update($request->all());
        return redirect()->route('customers.index')
            ->with('success', 'Customers Record data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();
        return redirect()->route('customers.index')
            ->with('success', 'Customer Record data deleted successfully');
    }
}

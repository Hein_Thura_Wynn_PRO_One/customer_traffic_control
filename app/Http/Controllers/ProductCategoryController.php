<?php

namespace App\Http\Controllers;
use App\Models\Product_Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:product-category-list|product-category-create|product-category-edit|product-category-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:product-category-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:product-category-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:product-category-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $product_Categories = Product_Category::orderBy('product_category_id', 'ASC')->paginate(15);
        return view('product_Categories.index', compact('product_Categories'))->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product_Categories.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'product_category_id' => 'required',
            'product_category_code' => 'required',
            'product_category_name' => 'required',
        ]);
        $request['created_by']  = Auth::id();
        Product_Category::create($request->all());
        return redirect()->route('product_Categories.index')
            ->with('success', 'Product_Category created successfully.');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product_Category  $product_Category
     * @return \Illuminate\Http\Response
     */
    public function show(Product_Category $product_Category)
    {
        return view('product_Categories.show', compact('product_Category'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product_Category  $product_Category
     * @return \Illuminate\Http\Response
     */
    public function edit(Product_Category $product_Category)
    {
        return view('product_Categories.edit', compact('product_Category'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product_Category  $product_Category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product_Category $product_Category)
    {
        request()->validate([
            'product_category_id' => 'required',
            'product_category_code' => 'required',
            'product_category_name' => 'required',
        ]);
        $update = [];
        if ($files = $request->file('img')) {
            $destinationPath = 'public/image/'; // upload path
            // $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $org_file_name = $files->getClientOriginalName();
            $extension = $files->getClientOriginalExtension();
            $filename = $org_file_name . $extension;
            $files->move($destinationPath, $filename);
            $update['img'] = "$org_file_name";
        }
        $update['product_category_id'] = $request->get('product_category_id');
        $update['product_category_code'] = $request->get('product_category_code');
        $update['product_category_name'] = $request->get('product_category_name');
        $update['created_by']  = Auth::id();
        $product_Category->update($request->all());
        return redirect()->route('product_Categories.index')
            ->with('success', 'Product_Category updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product_Category  $product_Category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_Category $product_Category)
    {
        $product_Category->delete();
        return redirect()->route('product_Categories.index')
            ->with('success', 'Product Category deleted successfully');
    }
}

@extends('adminlte::auth.auth-page', ['auth_type' => 'login'])

@php( $password_reset_url = View::getSection('password_reset_url') ?? config('adminlte.password_reset_url', 'password/reset') )

@if (config('adminlte.use_route_url', false))
@php( $password_reset_url = $password_reset_url ? route($password_reset_url) : '' )
@else
@php( $password_reset_url = $password_reset_url ? url($password_reset_url) : '' )
@endif

@section('auth_header', __('adminlte::adminlte.password_reset_message'))

@section('auth_body')

<form action="{{ route('reset_pwd') }}" method="POST">
    <!-- route('reset_pwd')  -->
    @csrf
    {{ csrf_field() }}

    {{-- ph_no field --}}
    <div class="input-group mb-3">

        <input type="tel" name="employee_id" class="form-control {{ $errors->has('employee_id') ? 'is-invalid' : '' }}" value="{{ old('employee_id') }}" placeholder="{{ __('Employee Code No') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-user {{ config('adminlte.classes_auth_icon', '') }}"></span>
            </div>
        </div>
        @if($errors->has('employee_id'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('employee_id') }}</strong>
        </div>
        @endif
    </div>

    <div class="input-group mb-3">

        <input type="tel" name="ph_no" class="form-control {{ $errors->has('ph_no') ? 'is-invalid' : '' }}" value="{{ old('ph_no') }}" placeholder="{{ __('Phone No') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-phone {{ config('adminlte.classes_auth_icon', '') }}"></span>
            </div>
        </div>
        @if($errors->has('ph_no'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('ph_no') }}</strong>
        </div>
        @endif
    </div>

    {{-- Password field --}}
    <div class="input-group mb-3">
        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.password') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
            </div>
        </div>
        @if($errors->has('password'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('password') }}</strong>
        </div>
        @endif
    </div>
    {{-- Confirm password field --}}
    <div class="input-group mb-3">
        <input type="password" name="confirm-password" class="form-control {{ $errors->has('confirm-password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.retype_password') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
            </div>
        </div>
        @if($errors->has('confirm-password'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('confirm-password') }}</strong>
        </div>
        @endif
    </div>
    {{-- Confirm password reset button --}}
    <button type="submit" class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
        <span class="fas fa-sync-alt"></span>
        {{ __('adminlte::adminlte.reset_password') }}
    </button>
</form>
@stop
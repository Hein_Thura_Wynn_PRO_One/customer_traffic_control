@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Design Group</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('product_Designs.index') }}"> Back</a>
        </div>
    </div>
</div>


@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('product_Designs.store') }}" method="POST" enctype="multipart/form-data">

    @csrf


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Design Id:</strong>
                <input type="text" name="product_design_id" class="form-control" value="{{ old('product_design_id') }}" placeholder="Product Design ID">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Desingn Code:</strong>
                <input type="text" name="product_design_code" class="form-control" value="{{ old('product_design_code') }}" placeholder="Product Design Code">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Desingn Name:</strong>
                <input type="text" name="product_group_name" class="form-control" value="{{ old('product_group_name') }}" placeholder="Product Design Name">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>


</form>


@endsection
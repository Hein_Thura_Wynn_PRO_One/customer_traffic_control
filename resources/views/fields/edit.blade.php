
@extends('layouts.app')
@section('content')
<div class="container bg-light">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Customer Field</h2>
            </div>
            <div class="pull-right mb-3">
                @can('field-edit')
                <a class="btn btn-primary" href="{{ route('fields.index') }}"> Back</a>
                @endcan
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <form action="{{ route('fields.update',$field->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="card-header">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                            <label class="control-label col-sm-4 pull-left">Name</label>
                                <input type="text" name="name" value="{{ $field->name }}" class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label col-sm-4 pull-left">Type</label>
                                <select class="form-control" name="type">
                                    @if ($field->type == 0)
                                    <option value="0" selected>{{ 'Customer' }}</option>
                                    <option value="1">{{ 'Vehical' }}</option>
                                    @elseif($field->type == 1)
                                    <option value="0">{{ 'Customer' }}</option>
                                    <option value="1" selected>{{ 'Vehical' }}</option>
                                    @else
                                    <option value="0">{{ 'Customer' }}</option>
                                    <option value="1">{{ 'Vehical' }}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
</div>
@endsection
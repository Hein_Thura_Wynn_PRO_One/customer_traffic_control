@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col">

        <div class="container mt-4 bg-light mb-3 card">
            <div class="row">
                <div class="col my-3">
                    <h1 class="text-primary text-uppercase font-weight-bold ">Customer Traffic Record</h1>
                </div>

                <div class="col my-3 text-right">
                    @can('customer-create')
                    <a class="btn btn-primary mt-3 " href="{{ route('customers.create') }}" title="Add Customer Traffic">Add <i class="fas fa-users text-dark"></i> <i class="fas fa-chart-line text-dark"></i> <i class="fas fa-plus"></i> </a>
                    @endcan
                </div>
            </div>
            <div class="row">

                @if ($message = Session::get('success'))

                <div class="col">
                    <p class=" alert alert-success">{{ $message }}</p>
                </div>
                @endif
                <div class="col-12 ">
                    <div class="card">
                        <!-- mb-3 <div class="card-header text-center font-weight-bold bg-primary">
                            <h2 class="text-light text-uppercase"> Search by <i class="fas fa-search text-warning"></i></h2>
                        </div> 
                        php $branch_count = count($branches);@endphp
                        { $branch_count == 1 ? 'selected': '' } -->
                        <div class="card-header bg-info">
                            <form action="{{ route('customers.search') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <button type="submit" class="btn btn-light"> <strong class="text-primary">Search</strong> <i class="fas fa-search text-info"></i></button>
                                        <a href="{{ route('customers.index') }}" class="btn btn-light"> <strong class="text-primary">Reset</strong> <i class="fas fa-redo text-danger"></i></a>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <strong class="text-light">Branch Name :</strong>
                                            <select class="form-control" id="branch_id" name="branch_id" focus>
                                                <option value="" disabled selected>-- Please select Branch --</option>
                                                @foreach($branches as $branch)

                                                <option value="{{$branch->id}}" {{ $branch->id == $branch_id  ? 'selected': '' }}>{{ $branch->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <strong class="text-light">From Date :</strong>
                                            <input type="date" id="from_date" name="from_date" class="form-control" value="{{$from_date}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <strong class="text-light">To Date :</strong>
                                            <input type="date" id="to_date" name="to_date" class="form-control" value="{{$to_date}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <strong class="text-light">Time Period :</strong>
                                            <select class="form-control" id="time" name="time">
                                                <option value="" disabled selected>-- Please select Time Period --</option>
                                                @foreach($periods as $period)
                                                <option value="{{$period->time}}" {{  $period->time == $time ? 'selected': '' }}>{{ $period->time }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12 table-responsive">
                    <table class="table table-bordered bg-primary mb-3">
                        <thead class="bg-primary text-light">
                            <tr>
                                <th rowspan="2" class="p-1">No</th>
                                <th rowspan="2" class="p-1">Branch Name</th>
                                <th rowspan="2" class="p-1">Date</th>
                                <th rowspan="2" class="p-1">Time</th>
                                <th colspan="{{count($customers_field)}}" class="text-center p-1">Customer</th>
                                <th rowspan="2" class="text-center  bg-warning text-dark p-1" width="50">Total Customer</th>
                                <th colspan="{{count($vehicles_field)}}" class="text-center p-1">Vehicles</th>
                                <th rowspan="2" class="text-center  bg-secondary p-1" width="50">Total Vehicles</th>
                                <th rowspan="2" class="p-1">Created By</th>
                                <th rowspan="2" class="p-1">Action</th>
                            </tr>
                            <tr>
                                @foreach ($customers_field as $field)
                                <th class="bg-warning text-dark">{{$field->name}} </th>
                                @endforeach
                                @foreach ($vehicles_field as $field)
                                <th class="bg-secondary"> {{$field->name}} </th>
                                @endforeach

                            </tr>
                        </thead>
                        <tbody class="bg-info">
                            @php
                            $tot_cus = 0 ;
                            $tot_veh = 0 ;
                            @endphp

                            <tr class="{{ count($customers) > 0 ? 'd-none' : '' }}">
                                <td class="{{ count($customers) > 0 ? 'd-none' : '' }} text-center text-light" colspan="100%">{{'No data found'}}</td>
                            </tr>
                            @foreach ($customers as $customer)
                            <tr>
                                <td class="p-1">{{ ++$i }}</td>
                                <td class="p-1">{{ $customer->branch_id ? $customer->branches->name : ""}}</td>
                                <td class="p-1">{{ $customer->date_client }}</td>
                                <td class="p-1">{{ $customer->time }}</td>
                                @php
                                $cus_i = 0;
                                $veh_i = 0;
                                @endphp
                                @foreach(json_decode($customer->customers, true) as $cus_key => $cus_value)

                                <td class="p-1 text-right">
                                    {{$cus_value}}
                                </td>
                                @endforeach

                                <td class="font-weight-bold text-warning p-1  text-right" width="50">{{$customer->tot_cus}}</td>
                                @foreach(json_decode($customer->vehicles, true) as $veh_key => $veh_value)

                                <td class="p-1  text-right">
                                    {{$veh_value}}
                                </td>
                                @endforeach

                                <td class="font-weight-bold text-secondary p-1  text-right" width="50">{{$customer->tot_veh}}</td>
                                <td class="p-1">{{ $customer->created_by ? $customer->user_name->name : "" }}</td>
                                @php
                                $tot_cus += $customer->tot_cus ;
                                $tot_veh += $customer->tot_veh ;
                                @endphp
                                <td>@can('customer-edit')
                                    <a class="btn btn-primary" href="{{ route('customers.edit',$customer->id) }}" width="100">Edit</a>
                                    @endcan
                                    @csrf
                                    @method('DELETE')
                                    @can('customer-delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                    @endcan
                                </td>

                            </tr>
                            @endforeach
                            <tr class="text-light bg-primary">
                                <th colspan="4" class="text-center p-1">Customer Traffic</th>
                                <th colspan="{{count($customers_field)}}" class="text-center p-1">Total Customers</th>
                                <th class="font-weight-bold p-1  text-right">{{$tot_cus}}</td>
                                <th class="text-center p-1" colspan="{{count($vehicles_field)}}">Total Vehicles</th>
                                <th class="font-weight-bold p-1  text-right">{{$tot_veh}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="text-md-right text-center mb-3 text-secondary">
                        {!! $customers->links() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
@section('js')
<script>
    $(document).ready(function() {
        from_date.max = new Date().toISOString().split("T")[0];
        to_date.max = new Date().toISOString().split("T")[0];

        // console.log( "ready!" );
    });
</script>
@endsection
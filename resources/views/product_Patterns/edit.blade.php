@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product Pattern</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product_Patterns.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('product_Patterns.update',$product_Pattern->id) }}" method="POST">
    	@csrf
        @method('PUT')


         <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Pattern Id:</strong>
		            <input type="text" name="product_pattern_id" value="{{ $product_Pattern->product_pattern_id }}" class="form-control" placeholder="Product Pattern Id">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Pattern Code:</strong> 
		            <input type="text" name="product_pattern_code" value="{{ $product_Pattern->product_pattern_code }}" class="form-control" placeholder="Product Pattern Code">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Pattern Name:</strong>
		            <input type="text" name="product_pattern_name" value="{{ $product_Pattern->product_pattern_name }}" class="form-control" placeholder="Product Pattern Name">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		      <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>


    </form>


@endsection
@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Product</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('product_Codes.index') }}"> Back</a>
        </div>
    </div>
</div>
@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form action="{{ route('product_Codes.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="offset-xs-10 offset-sm-10  offset-md-10 col-xs-2 col-sm-2 col-md-2">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save <i class="fa fab fa-save"></i></button>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Name :</label>

                <input type="text" name="product_name" class="form-control" value="{{ old('product_name') }}" placeholder="Name">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Prodcut Code:</label>
                <input type="text" name="product_code_no" class="form-control" value="{{ old('product_code_no') }}" placeholder="Barcode">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Product Type <span class="text-red">*</span>:</label>
                <select class="form-control" name="type" required focus>
                    <option value="">{{ '--HIP ? Structure--' }}</option>
                    @if (!empty(old('type')) && old('type') == 0)
                    <option value="0" selected>{{ 'HIP' }}</option>
                    <option value="1">{{ 'STRUCTURE' }}</option>
                    @elseif(!empty(old('type')) && old('type') == 1)
                    <option value="0">{{ 'HIP' }}</option>
                    <option value="1" selected>{{ 'STRUCTURE' }}</option>
                    @else
                    <option value="0">{{ 'HIP' }}</option>
                    <option value="1">{{ 'STRUCTURE' }}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Category Name <span class="text-red">*</span>:</label>
                <select class="form-control" id='category_id' name="{{'category_id'}}" required focus>
                    <option value="">{{ '--Select One Category--' }}</option>
                    @if($categories != null)
                    @foreach ($categories as $ln)
                    <option value="{{$ln->id}}" {{ old('category_id') == $ln->id ? 'selected': '' }}>{{ $ln->product_category_name }}</option>
                    @endforeach
                    @elseif ($category != null)
                    <option value="{{$category->id}}" {{ old('category_id') == $category->id ? 'selected': '' }}>{{ $category->product_category_name }}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Group Name <span class="text-red">*</span>:</label>
                <select class="form-control" id='group_id' name="{{'group_id'}}" required focus>
                    <option value="">{{ '--Select One Group--' }}</option>
                </select>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Pattern Name <span class="text-red">*</span>:</label>
                <select class="form-control" id='pattern_id' name="{{'pattern_id'}}" required>
                    <option value="">{{ '--Select One Pattern--' }}</option>
                </select>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Design Name <span class="text-red">*</span>:</label>
                <select class="form-control" id='design_id' name="{{'design_id'}}" required>
                    <option value="">{{ '--Select One Design--' }}</option>
                </select>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Unit Name <span class="text-red">*</span>:</label>
                <select class="form-control" required name="{{'unit_id'}}" required>
                    <option value="">{{ '--Select One Unit--' }}</option>
                    @foreach ($units as $ln)
                    <option value="{{$ln->id}}">{{ $ln->product_unit_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Vendor Name <span class="text-red">*</span>:</label>
                <select class="form-control" name="{{'supplier_id'}}" required>
                    <option value="">{{ '--Select One Vendor--' }}</option>
                    @foreach ($vendors as $ln)
                    <option value="{{$ln->id}}">{{ $ln->vendor_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Brand Name <span class="text-red">*</span>:</label>
                <select class="form-control" name="{{'brand_id'}}" required>
                    <option value=""> {{ '--Select One Brand--' }}</option>
                    @foreach ($brands as $ln)
                    <option value="{{$ln->id}}">{{ $ln->product_brand_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#category_id').change(function() {
            var id = $(this).val();
            $('#group_id').find('option').not(':first').remove();
            $.ajax({
                url: 'cat/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_group_id;
                            var name = response.data[i].product_group_name;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";

                            $("#group_id").append(option);
                        }
                    }
                }
            })
        });
        $('#group_id').change(function() {
            var id = $(this).val();
            $('#pattern_id').find('option').not(':first').remove();
            $.ajax({
                url: 'gp/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_pattern_id;
                            var name = response.data[i].product_pattern_name;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#pattern_id").append(option);
                        }
                    }
                }
            })
        });
        $('#pattern_id').change(function() {
            var id = $(this).val();
            $('#design_id').find('option').not(':first').remove();
            $.ajax({
                url: 'ptn/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_design_id;
                            var name = response.data[i].product_design_name;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#design_id").append(option);
                        }
                    }
                }
            })
        });
    });
</script>
@endsection
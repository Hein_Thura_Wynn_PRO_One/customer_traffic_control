@extends('layouts.app')
@section('content')
<div class="container bg-light">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Time</h2>
            </div>
            <div class="pull-right mb-3">
                @can('period-edit')
                <a class="btn btn-primary" href="{{ route('periods.index') }}"> Back</a>
                @endcan
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <form action="{{ route('periods.update',$period->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-header">

            <div class="form-group">
                <label for="exampleInputEmail1">Time</label>
                <input type="time" name="time1" value="time2" class="form-control" placeholder="Time">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Time</label>
                <input type="time" id="time2" name="time2" class="form-control" required>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
@extends('adminlte::page')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Post</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('posts.index') }}"> Back</a>
        </div>
    </div>
</div>


@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">

    @csrf


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group has-feedback {{ $errors->has('category_id') ? ' has-error' : '' }}">
                    <label class="control-label col-sm-2 pull-left"> Category Name <span
                            class="text-red">*</span>:</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="{{'category_id'}}">
                            <option>{{ '--Select One Location--' }}</option>
                              
                            @foreach ($categories as $ln)
                                {{--{{ ($ln->id == old('category_id') ? 'selected="selected"' : '') }}--}}
                                <option
                                    value="{{$ln->id}}" {{ ($ln->id == old('category_id') ? 'selected="selected"' : '') }} >{{ $ln->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('category_id'))
                            <span class="help-block">
                            {{--{{ $errors->first('category_id') }}--}}
                                {{ 'Select One Category' }}
                            </span>
                        @endif
                    </div>
                </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Image:</strong>
                <input type="file" name="image" class="form-control" placeholder="image">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Short Descripiton:</strong>
                <textarea class="form-control" style="height:150px" name="short_desc" placeholder="Short Descripiton"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Descripiton:</strong>
                <textarea class="form-control" style="height:150px" name="description" placeholder="Descripiton"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Key Pair:</strong>
                <textarea class="form-control" style="height:150px" name="key_pair" placeholder="Key:Pair"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>


</form>


@endsection